#!/usr/bin/make -f
# -*- makefile -*-

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

# necessary for override_dh_auto_test
PYVERS = $(shell pyversions -r -v)
pythonpath = $$(ls -d $(CURDIR)/build/lib.*-$(1))
pythonpath_dbg = $$(ls -d $(CURDIR)/build/lib_d.*-$(1) 2>/dev/null || ls -d $(CURDIR)/build/lib.*$(1)-pydebug)

%:
	dh $@ --with python2

# Workaround for #589759
override_dh_auto_build:
	dh_auto_build -- --force

override_dh_install:
	dh_install -X"*_d.so" "debian/tmp/*" -p python-geohash
	dh_install "debian/tmp/usr/lib/python*/*-packages/*_d.so" -p python-geohash-dbg
	dh_install

override_dh_strip:
ifeq (,$(filter nostrip,$(DEB_BUILD_OPTIONS)))
	dh_strip --dbg-package=python-geohash-dbg
endif

override_dh_auto_test:
ifeq (,$(findstring nocheck,$(DEB_BUILD_OPTIONS)))
	set -e ;\
	for py in $(PYVERS); do \
		for test in test/test_*.py; do \
			PYTHONPATH=$(call pythonpath,$$py) \
				python$$py $$test ;\
			PYTHONPATH=$(call pythonpath_dbg,$$py) \
				python$$py-dbg $$test ;\
		done \
	done
endif

# Workaround for #593382
override_dh_compress:
	dh_compress -Xexamples
